<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = ['name', 'slug', 'logo', 'email', 'phone', 'address', 'lat', 'lng'];

    public function scopeSearch($query, $term){
        return $query->where('id',$term)->orWhere('name','like',$term.'%')->orWhere( 'email', 'like', $term . '%')->orWhere( 'phone', 'like', $term . '%');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
}
