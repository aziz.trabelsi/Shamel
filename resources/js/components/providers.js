Vue.component('partners', {
    data() {
        return {
            providers: {},
            searchForm: new SparkForm({
                query: ''
            }),
            providerForm: new SparkForm({
                name: '',
                email: '',
                phone: '',
                address: '',
                logo: ''
            }),
            noProviders: false,
            searching: false,
            noSearchResults: false,
            searchResults: [],
            showingUserProfile: false,
            isSearch: false,
            isCreate: false,
            isUpdate: false
        }
    },
    computed: {
        /**
         * Calculate the style attribute for the photo preview.
         */

    },
    created() {
        this.getPartners();
    },

    methods: {
        previewStyle(logo_url) {
            return `background-image: url(${logo_url})`;
        },
        getPartners(page) {
            if (typeof page === 'undefined') {
                page = 1;
            }
            this.searching = true;
            axios.get('/api/v1/partners', {
                    page: page
                })
                .then(response => {
                    this.providers = response.data;
                    this.noProviders = this.providers.length == 0;
                });
        },
        prevPage() {
            this.getPartners(--this.providers.current_page);
        },
        nextPage() {
            this.getPartners(++this.providers.current_page);
        },
        selectPage(page) {
            this.getPartners(page);
        },

        createProvider() {
            this.creatingProvider = true;
            this.createProviderForm.url = '';

            $('#modal-create-provider').modal('show');
            setTimeout(() => {
                $('#modal-create-provider .on-focus').focus();
            }, 500);
        },
        store() {
            Spark.post('/api/v1/partners', this.createProviderForm)
                .then(() => {
                    this.creatingProvider = false;
                    this.getPartners();

                    $('#modal-create-provider').modal('hide');
                });
        },

        /**
         * Perform a search for the given query.
         */
        search() {
            if (this.searchForm.query.length > 3) {
                this.isSearch = true;
                this.searching = true;
                this.noSearchResults = false;

                axios.post('/api/v1/partners/search', this.searchForm)
                    .then(response => {
                        this.searchResults = response.data;
                        this.noSearchResults = this.searchResults.length === 0;
                        this.searching = false;
                    });
            } else if (this.searchForm.query.length == 0) {
                this.isSearch = false;
            }
        },
        /**
         * Perform a create a Provider.
         */
        create() {
            this.providers.data = {
                name: this.providerForm.name,
                address: this.providerForm.address,
                email: this.providerForm.email,
                phone: this.providerForm.phone
            };
            axios.post('/api/v1/partners', this.providers.data)
                .then(response => {
                    this.isCreate = false;
                });
        },

        /**
         * Perform a create a Provider.
         */
        update() {
            console.log(this.providerForm.name)
        },

        toIndex() {
            this.isCreate = false;
        },
        toCreate() {
            this.isCreate = true;
        },
        /**
         * Update the user's profile photo.
         */
        updateLogo(e) {
            e.preventDefault();

            if (!this.$refs.photo.files.length) {
                return;
            }

            var self = this;

            this.form.startProcessing();

            // We need to gather a fresh FormData instance with the profile photo appended to
            // the data so we can POST it up to the server. This will allow us to do async
            // uploads of the profile photos. We will update the user after this action.
            axios.post('api/v1/partners/logo', this.gatherFormData())
                .then(
                    () => {
                        //Bus.$emit('updateProvider');

                        self.form.finishProcessing();
                    },
                    (error) => {
                        self.form.setErrors(error.response.data.errors);
                    }
                );
        },


        /**
         * Gather the form data for the photo upload.
         */
        gatherFormData() {
            const data = new FormData();

            data.append('logo', this.$refs.logo.files[0]);

            return data;
        }
    },
});
