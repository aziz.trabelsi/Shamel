<!-- No Search Results -->
<div class="card card-default" v-if=" ! searching && noSearchResults">
    <div class="card-header">{{__('Search Results')}}</div>

    <div class="card-body">
        {{__('No providers matched the given criteria.')}}
    </div>
</div>

<!-- Provider Search Results -->
<div class="card card-default" v-if=" ! searching && ! noSearchResults">
    <div class="card-header">{{__('Search Results')}}</div>

    <div class="table-responsive">
        <table class="table table-valign-middle mb-0">
            <thead>
                <tr>
                    <th>{{__('Logo')}}</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Email')}}</th>
                    <th>{{__('Phone')}}</th>
                    <th class="th-fit"></th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="searchProvider in searchResults">

                    <td>
                        <img :src="searchProvider.logo" class="spark-profile-photo" alt="{{__('Logo')}}" />
                    </td>

                    <!-- Name -->
                    <td>
                        <div>
                            @{{ searchProvider.name }}
                        </div>
                    </td>

                    <!-- E-Mail Address -->
                    <td>
                        <div>
                            @{{ searchProvider.email }}
                        </div>
                    </td>

                    <!-- Phone -->
                    <td>
                        <div>
                            @{{ searchProvider.phone }}
                        </div>
                    </td>


                </tr>
            </tbody>
        </table>
    </div>
</div>
