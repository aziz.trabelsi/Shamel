<!-- Search Field card -->
<div class="card card-default" style="border: 0;">
    <div class="card-header">{{__('Search')}}</div>
    <div class="card-body">
        <form role="form" @submit.prevent>
            <!-- Search Field -->

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span>
                </div>
                <input type="text" class="form-control" aria-label="Username" id="providers-search" name="search"
                    placeholder="{{__('Search By Name Or E-Mail Address...')}}" v-model="searchForm.query"
                    @keyup="search" aria-describedby="basic-addon1">
            </div>

        </form>
    </div>
</div>
